<?php

namespace App\Controller;

use App\Repository\CategoriaRepository;
use App\Repository\MarcadorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class ZindexController extends AbstractController
{
    public const ELEMENTOS_POR_PAGINA = 5;

    /**
     * @Route(
     *  "/{categoria}/{pagina}",
     *  name="app_index",
     *  defaults={
     *      "categoria": "todas",
     *      "pagina": 1
     *  },
     *  requirements={
     *      "pagina"="\d+"
     *  },
     *  methods={
     *      "GET"
     *  },
     *  priority=2
     * )
     */
    public function index(
        string $categoria,
        int $pagina,
        CategoriaRepository $categoriaRepository,
        MarcadorRepository $marcadorRepository,
        TranslatorInterface $translator,
        Security $security)
    {
        $user = $security->getUser();
        if ($user) {
            $elementosPorPagina = self::ELEMENTOS_POR_PAGINA;

            $categoria = (int) $categoria > 0 ? (int)$categoria : $categoria;
            if(is_int($categoria)){
                $categoria = 'todas';
                $pagina = $categoria;
            }

            if ($categoria && 'todas' !== $categoria) {
                if (!$categoriaRepository->findBy([
                    'nombre' => $categoria,
                    'usuario' => $this->getUser()
                ])) {
                    throw $this->createNotFoundException($translator->trans("La categoría \"{categoria}\" no existe.", [
                        '{categoria}' => $categoria
                    ], 'messages'));
                }
                $marcadores = $marcadorRepository->buscarPorNombreCategoria($categoria, $pagina, $elementosPorPagina);
            } else {
                $marcadores = $marcadorRepository->buscarTodos($pagina, $elementosPorPagina);
            }

            return $this->render('index/index.html.twig', [
                'marcadores' => $marcadores,
                'pagina' => $pagina,
                'parametros_ruta' => [
                    'categoria' => $categoria
                ],
                'elementos_por_pagina' => $elementosPorPagina
            ]);
        } else {
            return $this->redirectToRoute('app_login');
        }
    }
}
