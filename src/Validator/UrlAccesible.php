<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UrlAccesible extends Constraint
{
    public $message = 'La url indicada no es accesible({{ codigo }}).';
}
