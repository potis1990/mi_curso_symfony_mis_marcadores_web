<?php

namespace App\DataFixtures;

use App\Entity\Categoria;
use App\Entity\Marcador;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class MarcadoresFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for($i=0; $i < 10; $i++){
            $marcador = new Marcador();
            $marcador->setNombre('Google' . $i);
            $marcador->setUrl('http://www.google.es');
            $marcador->setCategoria($this->getReference(CategoriasFixtures::CATEGORIA_INTERNET_REFERENCIA));
            $marcador->setUsuario($this->getReference(UsuariosFixtures::USUARIO_USER_REFERENCIA));
            $manager->persist($marcador);
        }
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UsuariosFixtures::class,
            CategoriasFixtures::class
        ];
    }
}
