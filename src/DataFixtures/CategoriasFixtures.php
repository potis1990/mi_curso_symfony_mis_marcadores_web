<?php

namespace App\DataFixtures;

use App\Entity\Categoria;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CategoriasFixtures extends Fixture  implements DependentFixtureInterface
{

    public const CATEGORIA_INTERNET_REFERENCIA = 'categoria-internet';

    public function load(ObjectManager $manager)
    {
        $categoria = new Categoria();
        $categoria->setNombre('Internet');
        $categoria->setColor('red');
        $categoria->setUsuario($this->getReference(UsuariosFixtures::USUARIO_USER_REFERENCIA));
        $manager->persist($categoria);
        $manager->flush();

        $this->addReference(self::CATEGORIA_INTERNET_REFERENCIA, $categoria);
    }

    public function getDependencies()
    {
        return [
            UsuariosFixtures::class
        ];
    }
}
