<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510085859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categoria ADD usuario_id INT NOT NULL');
        $this->addSql('ALTER TABLE categoria ADD CONSTRAINT FK_4E10122DDB38439E FOREIGN KEY (usuario_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_4E10122DDB38439E ON categoria (usuario_id)');
        $this->addSql('ALTER TABLE etiqueta ADD usuario_id INT NOT NULL');
        $this->addSql('ALTER TABLE etiqueta ADD CONSTRAINT FK_6D5CA63ADB38439E FOREIGN KEY (usuario_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_6D5CA63ADB38439E ON etiqueta (usuario_id)');
        $this->addSql('ALTER TABLE marcador ADD usuario_id INT NOT NULL, CHANGE favorito favorito TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE marcador ADD CONSTRAINT FK_B5F18E7DB38439E FOREIGN KEY (usuario_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_B5F18E7DB38439E ON marcador (usuario_id)');
        $this->addSql('ALTER TABLE marcador_etiqueta CHANGE marcador_id marcador_id INT DEFAULT NULL, CHANGE etiqueta_id etiqueta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE categoria DROP FOREIGN KEY FK_4E10122DDB38439E');
        $this->addSql('DROP INDEX IDX_4E10122DDB38439E ON categoria');
        $this->addSql('ALTER TABLE categoria DROP usuario_id');
        $this->addSql('ALTER TABLE etiqueta DROP FOREIGN KEY FK_6D5CA63ADB38439E');
        $this->addSql('DROP INDEX IDX_6D5CA63ADB38439E ON etiqueta');
        $this->addSql('ALTER TABLE etiqueta DROP usuario_id');
        $this->addSql('ALTER TABLE marcador DROP FOREIGN KEY FK_B5F18E7DB38439E');
        $this->addSql('DROP INDEX IDX_B5F18E7DB38439E ON marcador');
        $this->addSql('ALTER TABLE marcador DROP usuario_id, CHANGE favorito favorito TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE marcador_etiqueta CHANGE marcador_id marcador_id INT DEFAULT NULL, CHANGE etiqueta_id etiqueta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
