<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200511193354 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE marcador CHANGE favorito favorito TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE marcador_etiqueta CHANGE marcador_id marcador_id INT DEFAULT NULL, CHANGE etiqueta_id etiqueta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD activo TINYINT(1) NOT NULL, CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE marcador CHANGE favorito favorito TINYINT(1) DEFAULT \'NULL\'');
        $this->addSql('ALTER TABLE marcador_etiqueta CHANGE marcador_id marcador_id INT DEFAULT NULL, CHANGE etiqueta_id etiqueta_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user DROP activo, CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
