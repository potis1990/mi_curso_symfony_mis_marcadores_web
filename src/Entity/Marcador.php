<?php

namespace App\Entity;

use App\Validator as AppAssert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="App\Repository\MarcadorRepository")
 */
class Marcador
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Url
     * @AppAssert\UrlAccesible
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Categoria")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank
     */
    private $categoria;

    /**
     * @ORM\Column(type="datetime")
     * 
     */
    private $creado;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $favorito;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MarcadorEtiqueta", mappedBy="marcador")
     */
    private $marcadorEtiquetas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    public function __construct()
    {
        $this->etiqueta = new ArrayCollection();
        $this->marcadorEtiquetas = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist
     */
    public function setValorDefecto() {
        $this->creado = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getCategoria(): ?Categoria
    {
        return $this->categoria;
    }

    public function setCategoria(?Categoria $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    public function setCreado(\DateTimeInterface $creado): self
    {
        $this->creado = $creado;

        return $this;
    }

    public function getFavorito(): ?bool
    {
        return $this->favorito;
    }

    public function setFavorito(?bool $favorito): self
    {
        $this->favorito = $favorito;

        return $this;
    }

    /**
     * @return Collection|MarcadorEtiqueta[]
     */
    public function getMarcadorEtiquetas(): Collection
    {
        return $this->marcadorEtiquetas;
    }

    public function addMarcadorEtiqueta(MarcadorEtiqueta $marcadorEtiqueta): self
    {
        if (!$this->marcadorEtiquetas->contains($marcadorEtiqueta)) {
            $this->marcadorEtiquetas[] = $marcadorEtiqueta;
            $marcadorEtiqueta->setMarcador($this);
        }

        return $this;
    }

    public function removeMarcadorEtiqueta(MarcadorEtiqueta $marcadorEtiqueta): self
    {
        if ($this->marcadorEtiquetas->contains($marcadorEtiqueta)) {
            $this->marcadorEtiquetas->removeElement($marcadorEtiqueta);
            // set the owning side to null (unless already changed)
            if ($marcadorEtiqueta->getMarcador() === $this) {
                $marcadorEtiqueta->setMarcador(null);
            }
        }

        return $this;
    }

    public function getUsuario(): ?User
    {
        return $this->usuario;
    }

    public function setUsuario(?User $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

}
