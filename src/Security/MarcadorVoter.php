<?php 

namespace App\Security;

use App\Entity\Marcador;
use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use \Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class MarcadorVoter extends Voter
{
    const VER = 'ver';
    const EDITAR = 'editar';
    const ELIMINAR = 'eliminar';

    protected function supports(string $attribute, $subject)
    {
        if(!in_array($attribute, [self::VER, self::EDITAR, self::ELIMINAR]))
            return false;

        if(!$subject instanceof Marcador)
            return false;

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();

        if (!$user)
            return false;

        switch ($attribute) {
            case self::VER:
               return $this->puedeVer($subject, $user);
            break;
            case self::EDITAR:
                return $this->puedeEditar($subject, $user);
            break;
            case self::ELIMINAR:
                return $this->puedeEliminar($subject, $user);
            break;
        }
    }

    private function puedeVer(Marcador $marcador, User $usuario) {
        return $this->puedeEliminar($marcador, $usuario);
    }

    private function puedeEditar(Marcador $marcador, User $usuario) {
        return $this->puedeEliminar($marcador, $usuario);
    }

    private function puedeEliminar(Marcador $marcador, User $usuario) {
        return $usuario === $marcador->getUsuario();
    }

}